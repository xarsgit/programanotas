@extends('layouts.default')

@section('content')
<div class="container">
  <h3>Grado</h3>

  <div class="login">
    <form action="{{route('grado.store')}}" method="POST">
      {!! csrf_field() !!}
      <h3>Registro del Grado</h3><hr>
      <div class="input-group-box">
        <label>Nombre del Grado</label>
        <input type="text" name="nombre_grado" required>
      </div>
      <div class="input-group-box">
        <label>Jornada</label>
        <select name="jornada" required>
          <option value="MAÑANA" selected>Mañana</option>
          <option value="TARDE">Tarde</option>
          <option value="NOCHE">Noche</option>
        </select>
      </div>
      <div class="input-group-box">
        <label>Docente</label>
        <select name="docente_id">
          <option value="-1" selected>Sin seleccionar</option>
          @foreach($docentes as $docente)
          <option value="{{$docente->id}}">{{$docente->nombre_completo_docente}}</option>
          @endforeach
        </select>
      </div>
      <div class="buttons-box">
        <button type="button" class="red"><a href="{{route('grado.index')}}">Atras</a></button>
        <button type="submit">Guardar</button>
      </div>
    </form>
  </div>
</div>
@endsection