@extends('layouts.default')

@section('content')
<div class="container">
  <div class="top-box">
    <h3>Listado de Estudiantes del Grado ({{$grado->nombre_grado}}) a cargo del docente {{$docente->nombre_completo_docente}}</h3>
  </div>
  <div class="clear"></div>
  @if(count($estudiantes) > 0)
  <table>
    <thead>
      <tr>
        <th>Identificación</th>
        <th>Nombre</th>
        <th>Sexo</th>
        <th>Edad</th>
        <th>Acciones</th>
      </tr>
    </thead>
    <tbody>
      @foreach($estudiantes as $estudiante)
      <tr>
        <td>{{$estudiante->identificacion_estudiante}}</td>
        <td>{{$estudiante->nombre_completo_estudiante}}</td>
        <td>{{$estudiante->sexo_estudiante}}</td>
        <td>{{$estudiante->edad}}</td>
        <td>
          <button><a href="{{route('estudiante.edit', $estudiante->id)}}">Editar</a></button>
          {{-- <button><a href="{{route('estudiante.delete', $estudiante->id)}}">Eliminar</a></button> --}}
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  @else
  <div class="inside-box">
    <div class="box empty">
      <span>Sin resultados</span><br>
      <span>Por favor ingrese un nuevo registro</span>
    </div>
  </div>
  @endif
</div>
@endsection