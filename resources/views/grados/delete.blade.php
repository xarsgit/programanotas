@extends('layouts.default')

@section('content')
<div class="container">
  <h3>Grado</h3>

  <div class="login">
    <form action="{{route('grado.destroy', $grado->id)}}" id="deleteForm" method="POST">
      {!! csrf_field() !!}
      <h3>Eliminar Grado</h3><hr>
      <div class="input-group-box">
        <label>Identificación Grado</label>
        <input type="text" name="identificacion_grado" id="identificacion_grado" value="{{$grado->nombre_grado}}" readonly>
      </div>
      <div class="input-group-box">
        <label>Ingresa el nombre del grado para confirmar la eliminacion</label>
        <input type="text" name="identificacion_grado_2" id="identificacion_grado_2" required>
      </div>
      <div class="buttons-box">
        <button type="button" class="red"><a href="{{route('grado.index')}}">Atras</a></button>
        <button type="submit">Guardar</button>
      </div>
    </form>
  </div>
</div>
<script>
  document.addEventListener('DOMContentLoaded', function(event) {
    const realVal = document.getElementById('identificacion_grado').value;
    const form = document.getElementById('deleteForm');
    form.addEventListener("submit", submitForm);
    function submitForm(e) {
      const val = document.getElementById('identificacion_grado_2').value;
      if(realVal != val) {
        console.log("NOPE");
        e.preventDefault();
      }
    }
  });
</script>
@endsection