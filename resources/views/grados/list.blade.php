@extends('layouts.default')

@section('content')
<div class="container">
  <div class="top-box">
    <h3>Listado de Grados</h3>
    <a href="{{route('grado.create')}}"><button>Nuevo grado</button></a>
  </div>
  <div class="clear"></div>
  @if(count($grados) > 0)
  <table>
    <thead>
      <tr>
        <th>Nombre del Grado</th>
        <th>Docente</th>
        <th>Cantidad de Estudiantes</th>
        <th>Acciones</th>
      </tr>
    </thead>
    <tbody>
      @foreach($grados as $grado)
      <tr>
        <td>{{$grado->nombre_grado}}</td>
        <td>{{$grado->docente->nombre_completo_docente}}</td>
        <td>{{count($grado->estudiantes)}}</td>
        <td>
          <button><a href="{{route('grado.show', $grado->id)}}">Estudiantes</a></button>
          <button><a href="{{route('grado.edit', $grado->id)}}">Editar</a></button>
          <button><a href="{{route('grado.delete', $grado->id)}}">Eliminar</a></button>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  @else
  <div class="inside-box">
    <div class="box empty">
      <span>Sin resultados</span><br>
      <span>Por favor ingrese un nuevo registro</span>
    </div>
  </div>
  @endif
</div>
@endsection