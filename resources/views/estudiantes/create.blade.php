@extends('layouts.default')

@section('content')
<div class="container">
  <h3>Estudiante</h3>

  <div class="login">
    <form action="{{route('estudiante.store')}}" method="POST">
      {!! csrf_field() !!}
      <h3>Registro del Estudiante</h3><hr>
      {{-- <div class="input-group-box">
        <label>Colegio</label>
        <select name="colegio_id" required>
          @foreach($colegios as $colegio)
          <option value="{{$colegio->id}}">{{$colegio->nombre_colegio}}</option>
          @endforeach
        </select>
      </div> --}}
      <div class="input-group-box">
        <label>Identificación Estudiante</label>
        <input type="text" name="identificacion_estudiante" required>
      </div>
      <div class="input-group-box">
        <label>Nombre del Estudiante</label>
        <input type="text" name="nombre_estudiante" required>
      </div>
      <div class="input-group-box">
        <label>Apellido del Estudiante</label>
        <input type="text" name="apellido_estudiante" required>
      </div>
      <div class="input-group-box">
        <label>Sexo del Estudiante</label>
        <select name="sexo_estudiante" required>
          <option value="MASCULINO" selected>Masculino</option>
          <option value="FEMENINO">Femenino</option>
        </select>
      </div>
      <div class="input-group-box">
        <label>Fecha de Nacimiento</label>
        <input type="date" name="fecha_nacimiento_estudiante" required>
      </div>
      <div class="input-group-box">
        <label>Acudiente</label>
        <select name="acudiente_id">
          <option value="-1" selected>Sin seleccionar</option>
          @foreach($acudientes as $acudiente)
          <option value="{{$acudiente->id}}">{{$acudiente->nombre_completo_acudiente}}</option>
          @endforeach
        </select>
      </div>
      <div class="input-group-box">
        <label>Grado</label>
        <select name="grado_id">
          <option value="-1" selected>Sin seleccionar</option>
          @foreach($grados as $grado)
          <option value="{{$grado->id}}">{{$grado->nombre_grado}} - {{$grado->docente->nombre_completo_docente}}</option>
          @endforeach
        </select>
      </div>
      <div class="buttons-box">
        <button type="button" class="red"><a href="{{route('estudiante.index')}}">Atras</a></button>
        <button type="submit">Guardar</button>
      </div>
    </form>
  </div>
</div>
@endsection