@extends('layouts.default')

@section('content')
<div class="container">
  <h3>Estudiante</h3>

  <div class="login">
    <form action="{{route('estudiante.destroy', $estudiante->id)}}" id="deleteForm" method="POST">
      {!! csrf_field() !!}
      <h3>Eliminar Estudiante</h3><hr>
      <div class="input-group-box">
        <label>Identificación Estudiante</label>
        <input type="text" name="identificacion_estudiante" id="identificacion_estudiante" value="{{$estudiante->identificacion_estudiante}}" readonly>
      </div>
      <div class="input-group-box">
        <label>Ingresa el número de identificación del estudiante para confirmar la eliminacion</label>
        <input type="text" name="identificacion_estudiante_2" id="identificacion_estudiante_2" required>
      </div>
      <div class="buttons-box">
        <button type="button" class="red"><a href="{{route('estudiante.index')}}">Atras</a></button>
        <button type="submit">Guardar</button>
      </div>
    </form>
  </div>
</div>
<script>
  document.addEventListener('DOMContentLoaded', function(event) {
    const realVal = document.getElementById('identificacion_estudiante').value;
    const form = document.getElementById('deleteForm');
    form.addEventListener("submit", submitForm);
    function submitForm(e) {
      const val = document.getElementById('identificacion_estudiante_2').value;
      if(realVal != val) {
        console.log("NOPE");
        e.preventDefault();
      }
    }
  });
</script>
@endsection