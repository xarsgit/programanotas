@extends('layouts.default')

@section('content')
<div class="container">
  <h3>Estudiante</h3>

  <div class="login">
    <form action="{{route('estudiante.update', $estudiante->id)}}" method="POST">
      {!! csrf_field() !!}
      <h3>Edición del Estudiante</h3><hr>
      <div class="input-group-box">
        <label>Identificación Estudiante</label>
        <input type="text" name="identificacion_estudiante" value="{{$estudiante->identificacion_estudiante}}" required>
      </div>
      <div class="input-group-box">
        <label>Nombre del Estudiante</label>
        <input type="text" name="nombre_estudiante" value="{{$estudiante->nombre_estudiante}}" required>
      </div>
      <div class="input-group-box">
        <label>Apellido del Estudiante</label>
        <input type="text" name="apellido_estudiante" value="{{$estudiante->apellido_estudiante}}" required>
      </div>
      <div class="input-group-box">
        <label>Sexo del Estudiante</label>
        <select name="sexo_estudiante" required>
          @if($estudiante->sexo_estudiante == 'MASCULINO')
          <option value="MASCULINO" selected>Masculino</option>
          <option value="FEMENINO">Femenino</option>
          @else
          <option value="MASCULINO">Masculino</option>
          <option value="FEMENINO" selected>Femenino</option>
          @endif
        </select>
      </div>
      <div class="input-group-box">
        <label>Fecha de Nacimiento</label>
        <input type="date" name="fecha_nacimiento_estudiante" value="{{$estudiante->fecha_nacimiento_estudiante}}" required>
      </div>
      <div class="input-group-box">
        <label>Acudiente</label>
        <select name="acudiente_id">
          <option value="-1" selected>Sin seleccionar</option>
          @foreach($acudientes as $acudiente)
          @if($acudiente->id == $estudiante->acudiente_id)
          <option value="{{$acudiente->id}}" selected>{{$acudiente->nombre_completo_acudiente}}</option>
          @else
          <option value="{{$acudiente->id}}">{{$acudiente->nombre_completo_acudiente}}</option>
          @endif
          @endforeach
        </select>
      </div>
      <div class="input-group-box">
        <label>Grado</label>
        <select name="grado_id">
          <option value="-1" selected>Sin seleccionar</option>
          @foreach($grados as $grado)
          @if($grado->id == $estudiante->grado_id)
          <option value="{{$grado->id}}" selected>{{$grado->nombre_grado}} - {{$grado->docente->nombre_completo_docente}}</option>
          @else
          <option value="{{$grado->id}}">{{$grado->nombre_grado}} - {{$grado->docente->nombre_completo_docente}}</option>
          @endif
          @endforeach
        </select>
      </div>
      <div class="buttons-box">
        <button type="button" class="red"><a href="{{route('estudiante.index')}}">Atras</a></button>
        <button type="submit">Guardar</button>
      </div>
    </form>
  </div>
</div>
@endsection