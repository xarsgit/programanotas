<html>
<head>
  <title>Notas Digitales</title>
  <link href="{{asset('css/estilo.css')}}" rel="stylesheet" />
  <link href="{{asset('css/estilo2.css')}}" rel="stylesheet" />
  <link href="{{asset('css/estilo3.css')}}" rel="stylesheet" />

<body>
  <h1 class="main-title">{{Auth::user()->nombre_colegio}}</h1>

  <nav style="text-align: center;">
    <h3 class="school-name">MENÚ PRINCIPAL</h3>
    <button class="white"><a href="{{route('estudiante.index')}}"><img src="{{asset('Imagen/Estudiante.png')}}" alt="estudiante">Estudiante</a></button>
    <button class="white"><a href="{{route('acudiente.index')}}"><img src="{{asset('Imagen/Padres.png')}}" alt="estudiante">Acudiente</a></button>
    <button class="white"><a href="{{route('docente.index')}}"><img src="{{asset('Imagen/Docentes.jpg')}}" alt="estudiante">Docentes</a></button>
    <button class="white"><a href="{{route('grado.index')}}"><img src="{{asset('Imagen/Grado.png')}}" alt="estudiante">Grado</a></button>
    {{-- <button class="white"><a href="{{route('colegio.index')}}"><img src="{{asset('Imagen/Colegio.jpg')}}" alt="estudiante">Colegio</a></button> --}}
    <button class="white"><a style="display: block;" href="{{route('logout')}}">Salir</a></button>
  </nav>
  @yield('content')
  <footer>
    <div class="left">
      <ul>
        <li>Web</li>
        <li>Contáctanos</li>
      </ul>
    </div>
    <div class="right">
      <span>@2021</span>
    </div>
  </footer>
</body>
</head>
</html>