@extends('layouts.default')

@section('content')
<div class="container">
  <h3>Colegio</h3>
  <h5>{{Auth::user()->nombre_colegio}}</h5>
  <span>{{Auth::user()->nit_colegio}}</span>
  <p>{{Auth::user()->direccion}}</p>
</div>
@endsection