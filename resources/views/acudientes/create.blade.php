@extends('layouts.default')

@section('content')
<div class="container">
  <h3>Acudiente</h3>

  <div class="login">
    <form action="{{route('acudiente.store')}}" method="POST">
      {!! csrf_field() !!}
      <h3>Registro del Acudiente</h3><hr>
      <div class="input-group-box">
        <label>Cédula del Acudiente</label>
        <input type="text" name="cedula_acudiente" required>
      </div>
      <div class="input-group-box">
        <label>Nombre del Acudiente</label>
        <input type="text" name="nombre_acudiente" required>
      </div>
      <div class="input-group-box">
        <label>Apellido del Acudiente</label>
        <input type="text" name="apellido_acudiente" required>
      </div>
      <div class="input-group-box">
        <label>Dirección del Acudiente</label>
        <input type="text" name="direccion_residencia" required>
      </div>
      <div class="input-group-box">
        <label>Teléfono 1</label>
        <input type="number" name="telefono_1" required>
      </div>
      <div class="input-group-box">
        <label>Teléfono 2 (Opcional)</label>
        <input type="number" name="telefono_2">
      </div>
      <div class="buttons-box">
        <button type="button" class="red"><a href="{{route('acudiente.index')}}">Atras</a></button>
        <button type="submit">Guardar</button>
      </div>
    </form>
  </div>
</div>
@endsection