@extends('layouts.default')

@section('content')
<div class="container">
  <h3>Acudiente</h3>

  <div class="login">
    <form action="{{route('acudiente.destroy', $acudiente->id)}}" id="deleteForm" method="POST">
      {!! csrf_field() !!}
      <h3>Eliminar Acudiente</h3><hr>
      <div class="input-group-box">
        <label>Identificación Acudiente</label>
        <input type="text" name="identificacion_acudiente" id="identificacion_acudiente" value="{{$acudiente->cedula_acudiente}}" readonly>
      </div>
      <div class="input-group-box">
        <label>Ingresa el número de identificación del acudiente para confirmar la eliminacion</label>
        <input type="text" name="identificacion_acudiente_2" id="identificacion_acudiente_2" required>
      </div>
      <div class="buttons-box">
        <button type="button" class="red"><a href="{{route('acudiente.index')}}">Atras</a></button>
        <button type="submit">Guardar</button>
      </div>
    </form>
  </div>
</div>
<script>
  document.addEventListener('DOMContentLoaded', function(event) {
    const realVal = document.getElementById('identificacion_acudiente').value;
    const form = document.getElementById('deleteForm');
    form.addEventListener("submit", submitForm);
    function submitForm(e) {
      const val = document.getElementById('identificacion_acudiente_2').value;
      if(realVal != val) {
        console.log("NOPE");
        e.preventDefault();
      }
    }
  });
</script>
@endsection