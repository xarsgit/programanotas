@extends('layouts.default')

@section('content')
<div class="container">
  <div class="top-box">
    <h3>Listado de Acudientes</h3>
    <a href="{{route('acudiente.create')}}"><button>Nuevo acudiente</button></a>
  </div>
  <div class="clear"></div>
  <form action="{{route('acudiente.search')}}" id="searchForm" method="POST">
    {!! csrf_field() !!}
    <input type="text" name="search" value="{{$search}}" placeholder="Identificacion" required>
    <button type="submit">Buscar</button>
  </form>
  @if(count($acudientes) > 0)
  <table>
    <thead>
      <tr>
        <th>Identificación</th>
        <th>Nombre</th>
        <th>Dirección</th>
        <th>Teléfonos</th>
        <th>Acciones</th>
      </tr>
    </thead>
    <tbody>
      @foreach($acudientes as $acudiente)
      <tr>
        <td>{{$acudiente->cedula_acudiente}}</td>
        <td>{{$acudiente->nombre_completo_acudiente}}</td>
        <td>{{$acudiente->direccion_residencia}}</td>
        <td>
          {{$acudiente->telefonos[0]->telefono}}
          @if(count($acudiente->telefonos) > 1)
          <br>
          {{$acudiente->telefonos[1]->telefono}}
          @endif
        </td>
        <td>
          <button><a href="{{route('acudiente.edit', $acudiente->id)}}">Editar</a></button>
          <button><a href="{{route('acudiente.delete', $acudiente->id)}}">Eliminar</a></button>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  @else
  <div class="inside-box">
    <div class="box empty">
      <span>Sin resultados</span><br>
      <span>Por favor ingrese un nuevo registro</span>
    </div>
  </div>
  @endif
</div>
@endsection