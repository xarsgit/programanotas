<html>

<head>
  <title>Notas Digitales</title>
  <link href="{{asset('css/estilo.css')}}" rel="stylesheet" />
  <link href="{{asset('css/estilo2.css')}}" rel="stylesheet" />
  <body>
  <h1 class="main-title">NOTAS DIGITALES</h1>
  <div>
    <div class="izq">
      <img src="{{asset('Imagen/logo1.jpg')}}" alt="...">
    </div>
    <div class="der">
      <form action="{{route('login.store')}}" method="POST">
        {!! csrf_field() !!}
        <div class="borde">
          <label style="font-weight: bold;">Programa de Colegios</label>
        </div>
        <hr>
        <div class="login">
          <h3>Iniciar Sesión</h3><hr>
          <div class="input-group-box">
            <label>Usuario:</label>
            <select name="nombre_usuario" required>
              @foreach($colegios as $c)
              <option value="{{$c->nombre_usuario}}">{{$c->nombre_colegio}}</option>
              @endforeach
            </select>
          </div>
          <div class="input-group-box">
            <label>Contaseña</label>
            <input type="password" name="clave" required>
          </div>
          <div class="buttons-box login-button">
            <button type="submit">Autenticar</button>
          </div>
        </div>
      </form>
    </div>
  </div>
  <footer>
    <div class="left">
      <ul>
        <li>Web</li>
        <li>Contáctanos</li>
      </ul>
    </div>
    <div class="right">
      <span>@2021</span>
    </div>
  </footer>
</body>
</head>

</html>