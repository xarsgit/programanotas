@extends('layouts.default')

@section('content')
<div class="container">
  <h3>Docente</h3>

  <div class="login">
    <form action="{{route('docente.destroy', $docente->id)}}" id="deleteForm" method="POST">
      {!! csrf_field() !!}
      <h3>Eliminar Docente</h3><hr>
      <div class="input-group-box">
        <label>Identificación Docente</label>
        <input type="text" name="identificacion_docente" id="identificacion_docente" value="{{$docente->cedula_docente}}" readonly>
      </div>
      <div class="input-group-box">
        <label>Ingresa el número de identificación del docente para confirmar la eliminacion</label>
        <input type="text" name="identificacion_docente_2" id="identificacion_docente_2" required>
      </div>
      <div class="buttons-box">
        <button type="button" class="red"><a href="{{route('docente.index')}}">Atras</a></button>
        <button type="submit">Guardar</button>
      </div>
    </form>
  </div>
</div>
<script>
  document.addEventListener('DOMContentLoaded', function(event) {
    const realVal = document.getElementById('identificacion_docente').value;
    const form = document.getElementById('deleteForm');
    form.addEventListener("submit", submitForm);
    function submitForm(e) {
      const val = document.getElementById('identificacion_docente_2').value;
      if(realVal != val) {
        console.log("NOPE");
        e.preventDefault();
      }
    }
  });
</script>
@endsection