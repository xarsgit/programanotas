@extends('layouts.default')

@section('content')
<div class="container">
  <div class="top-box">
    <h3>Listado de Docentes</h3>
    <a href="{{route('docente.create')}}"><button>Nuevo docente</button></a>
  </div>
  <div class="clear"></div>
  <form action="{{route('docente.search')}}" id="searchForm" method="POST">
    {!! csrf_field() !!}
    <input type="text" name="search" value="{{$search}}" placeholder="Identificacion" required>
    <button type="submit">Buscar</button>
  </form>
  @if(count($docentes) > 0)
  <table>
    <thead>
      <tr>
        <th>Identificación</th>
        <th>Nombres</th>
        <th>Apellidos</th>
        <th>Acciones</th>
      </tr>
    </thead>
    <tbody>
      @foreach($docentes as $docente)
      <tr>
        <td>{{$docente->cedula_docente}}</td>
        <td>{{$docente->nombre_docente}}</td>
        <td>{{$docente->apellido_docente}}</td>
        <td>
          <button><a href="{{route('docente.edit', $docente->id)}}">Editar</a></button>
          <button><a href="{{route('docente.delete', $docente->id)}}">Eliminar</a></button>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  @else
  <div class="inside-box">
    <div class="box empty">
      <span>Sin resultados</span><br>
      <span>Por favor ingrese un nuevo registro</span>
    </div>
  </div>
  @endif
</div>
@endsection