@extends('layouts.default')

@section('content')
<div class="container">
  <h3>Docente</h3>

  <div class="login">
    <form action="{{route('docente.update', $docente->id)}}" method="POST">
      {!! csrf_field() !!}
      <h3>Registro del Docente</h3><hr>
      <div class="input-group-box">
        <label>Cédula del Docente</label>
        <input type="text" name="cedula_docente" value="{{$docente->cedula_docente}}" required>
      </div>
      <div class="input-group-box">
        <label>Nombre del Docente</label>
        <input type="text" name="nombre_docente" value="{{$docente->nombre_docente}}" required>
      </div>
      <div class="input-group-box">
        <label>Apellido del Docente</label>
        <input type="text" name="apellido_docente" value="{{$docente->nombre_docente}}" required>
      </div>
      <div class="buttons-box">
        <button type="button" class="red"><a href="{{route('docente.index')}}">Atras</a></button>
        <button type="submit">Guardar</button>
      </div>
    </form>
  </div>
</div>
@endsection