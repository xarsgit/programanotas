<html>
<head>
  <title>Notas Digitales</title>
  <link href="{{asset('css/estilo.css')}}" rel="stylesheet" />
  <link href="{{asset('css/estilo2.css')}}" rel="stylesheet" />
  <link href="{{asset('css/estilo3.css')}}" rel="stylesheet" />
  <style>
    label {
      font-weight: bold;
    }
  </style>
<body>
  <h1 class="main-title">NOTAS DIGITALES</h1>

  <h3 class="school-name">MENÚ PRINCIPAL</h3>
  <nav>
    <a href="{{route('estudiante.index')}}"><button><img src="{{asset('Imagen/Estudiante.png')}}" alt="estudiante">Estudiante</button></a>
    <a href="{{route('acudiente.index')}}"><button><img src="{{asset('Imagen/Padres.png')}}" alt="estudiante">Acudiente</button></a>
    <a href="{{route('docente.index')}}"><button><img src="{{asset('Imagen/Docentes.jpg')}}" alt="estudiante">Docentes</button></a>
    <a href="{{route('grado.index')}}"><button><img src="{{asset('Imagen/Grado.png')}}" alt="estudiante">Grado</button></a>
    {{-- <a href="{{route('colegio.index')}}"><button><img src="{{asset('Imagen/Colegio.jpg')}}" alt="estudiante">Colegio</button></a> --}}
  </nav>

  <div class="container">
    <h3>Colegios</h3>
    <a href="{{route('colegio.create')}}"><button style="float: left;">Nuevo colegio</button></a>
    <div class="login">
      @foreach($colegios as $colegio)
      <div class="box">
        <label>Nombre del colegio</label>
        <span>{{$colegio->nombre_colegio}}</span>
      </div>
      <div class="box">
        <label>NIT del colegio</label>
        <span>{{$colegio->nit_colegio}}</span>
      </div>
      <div class="box">
        <label>Dirección del colegio</label>
        <span>{{$colegio->direccion}}</span>
      </div>
      <br>
      <hr>
      @endforeach
    </div>
  </div>

  <footer>
    <div class="left">
      <ul>
        <li>Web</li>
        <li>Contáctanos</li>
      </ul>
    </div>
    <div class="right">
      <span>@2021</span>
    </div>
  </footer>
</body>
</head>
</html>