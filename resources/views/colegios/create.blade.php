<html>
<head>
  <title>Notas Digitales</title>
  <link href="{{asset('css/estilo.css')}}" rel="stylesheet" />
  <link href="{{asset('css/estilo2.css')}}" rel="stylesheet" />
  <link href="{{asset('css/estilo3.css')}}" rel="stylesheet" />

<body>
  <h1 class="main-title">NOTAS DIGITALES</h1>

  <h3 class="school-name">MENÚ PRINCIPAL</h3>
  <nav>
    <a href="{{route('estudiante.index')}}"><button><img src="{{asset('Imagen/Estudiante.png')}}" alt="estudiante">Estudiante</button></a>
    <a href="{{route('acudiente.index')}}"><button><img src="{{asset('Imagen/Padres.png')}}" alt="estudiante">Acudiente</button></a>
    <a href="{{route('docente.index')}}"><button><img src="{{asset('Imagen/Docentes.jpg')}}" alt="estudiante">Docentes</button></a>
    <a href="{{route('grado.index')}}"><button><img src="{{asset('Imagen/Grado.png')}}" alt="estudiante">Grado</button></a>
    {{-- <a href="{{route('colegio.index')}}"><button><img src="{{asset('Imagen/Colegio.jpg')}}" alt="estudiante">Colegio</button></a> --}}
  </nav>

  <div class="container">
    <h3>Colegio</h3>

    <div class="login">
      <form action="{{route('colegio.store')}}" method="POST">
        {!! csrf_field() !!}
        <h3>Registro del Colegio</h3>
        <br><br>
        <label>Nombre del Colegio</label>
        <input type="text" name="nombre_colegio">
        <br><br>
        <label>Nit del Colegio</label>
        <input type="text" name="nit_colegio">
        <br><br>
        <label>Dirección del Colegio</label>
        <input type="text" name="direccion">
        <br><br>
        <input type="submit" value="Guardar">
      </form>
      <br><br>
    </div>

  </div>

  <footer>
    <div class="left">
      <ul>
        <li>Web</li>
        <li>Contáctanos</li>
      </ul>
    </div>
    <div class="right">
      <span>@2021</span>
    </div>
  </footer>
</body>
</head>
</html>