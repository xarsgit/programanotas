<html>
<head>
  <title>Notas Digitales</title>
  <link href="{{asset('css/estilo.css')}}" rel="stylesheet" />
  <link href="{{asset('css/estilo2.css')}}" rel="stylesheet" />
  <link href="{{asset('css/estilo3.css')}}" rel="stylesheet" />

<body>
  <h1 class="main-title">NOTAS DIGITALES</h1>

  <h3 class="school-name">MENÚ PRINCIPAL</h3>
  <nav>
    {{-- <a href="{{route('estudiante.index')}}"><button><img src="{{asset('Imagen/Estudiante.png')}}" alt="estudiante">Estudiante</button></a> --}}
    <a href="{{route('acudiente.index')}}"><button><img src="{{asset('Imagen/Padres.png')}}" alt="estudiante">Acudiente</button></a>
    <a href="{{route('docente.index')}}"><button><img src="{{asset('Imagen/Docentes.jpg')}}" alt="estudiante">Docentes</button></a>
    <a href="{{route('grado.index')}}"><button><img src="{{asset('Imagen/Grado.png')}}" alt="estudiante">Grado</button></a>
  </nav>

  <div class="container">
    <h3>Estudiante</h3>

    <div class="login">
      <form action="{{route('estudiante.update', $estudiante->id)}}" method="POST">
        {!! csrf_field() !!}
        <h3>Registro del Estudiante</h3>
        <hr>
        <label>Identificación Estudiante</label>
        <input type="text" name="identificacion_estudiante" value="{{$estudiante->identificacion_estudiante}}">
        <br><br>
        <label>Nombre del Estudiante:</label>
        <input type="text" name="nombre_estudiante" value="{{$estudiante->nombre_estudiante}}">
        <br><br>
        <label>Apellido del Estudiante</label>
        <input type="text" name="apellido_estudiante" value="{{$estudiante->apellido_estudiante}}">
        <br><br>
        <label>Sexo del Estudiante:</label>
        <select type="text" name="sexo_estudiante">
          @if($estudiante->sexo_estudiante == 'MASCULINO')
          <option value="MASCULINO" selected>Masculino</option>
          <option value="FEMENINO">Femenino</option>
          @else
          <option value="MASCULINO">Masculino</option>
          <option value="FEMENINO" selected>Femenino</option>
          @endif
        </select>
        <br><br>
        <label>Fecha de Nacimiento delEstudiante</label>
        <input type="date" name="fecha_nacimiento_estudiante" value="{{$estudiante->fecha_nacimiento_estudiante}}">
        <br><br>
        <input type="submit" value="Guardar">
      </form>
      <br><br>
    </div>

  </div>

  <footer>
    <div class="left">
      <ul>
        <li>Web</li>
        <li>Contáctanos</li>
      </ul>
    </div>
    <div class="right">
      <span>@2021</span>
    </div>
  </footer>
</body>
</head>
</html>