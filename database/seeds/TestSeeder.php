<?php

use Illuminate\Database\Seeder;

class TestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('estudiantes')->insert([
            [
                'identificacion_estudiante' => '67455664',
                'nombre_estudiante' => 'Sergio Andres',
                'apellido_estudiante' => 'Robles Serrano',
                'sexo_estudiante' => 'MASCULINO',
                'fecha_nacimiento_estudiante' => '1997-04-23',
                'acudiente_id' => null,
                'colegio_id' => 1,
            ],
            [
                'identificacion_estudiante' => '23454667',
                'nombre_estudiante' => 'Estefany Paola',
                'apellido_estudiante' => 'Yepes Delgado',
                'sexo_estudiante' => 'FEMENINO',
                'fecha_nacimiento_estudiante' => '1997-07-08',
                'acudiente_id' => null,
                'colegio_id' => 1,
            ],
        ]);
        DB::table('acudientes')->insert([
            [
                'cedula_acudiente' => '768445674',
                'nombre_acudiente' => 'Carmen Teresa',
                'apellido_acudiente' => 'Serrano Castro',
                'direccion_residencia' => 'Carrera 19 B#343-23',
                'colegio_id' => 1,
            ],
        ]);
        DB::table('telefonos')->insert([
            [
                'telefono' => '3148640237',
                'acudiente_id' => 1,
            ],
        ]);
        DB::table('docentes')->insert([
            [
                'cedula_docente' => '23455265',
                'nombre_docente' => 'Eduardo Junior',
                'apellido_docente' => 'Robles Panetta',
                'colegio_id' => 1,
            ],
            [
                'cedula_docente' => '34563465',
                'nombre_docente' => 'Luis Carlos',
                'apellido_docente' => 'Serrano Castro',
                'colegio_id' => 1,
            ],
        ]);
        DB::table('grados')->insert([
            [
                'nombre_grado' => '7B',
                'jornada' => 'MAÑANA',
                'docente_id' => 1,
                'colegio_id' => 1,
            ],
            [
                'nombre_grado' => '10C',
                'jornada' => 'TARDE',
                'docente_id' => 2,
                'colegio_id' => 1,
            ],
        ]);
    }
}
