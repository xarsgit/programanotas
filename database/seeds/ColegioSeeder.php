<?php

use Illuminate\Database\Seeder;

class ColegioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('colegios')->insert([
            [
                'nombre_colegio' => 'Colegio Numero 1',
                'nit_colegio' => '123',
                'direccion' => 'Dirección de Prueba',
                'nombre_usuario' => '1',
                'clave' => '$2y$10$WGrQrtF9gVGu9hQFoYQ47eLz/YWABLJ3FO5YF36c9tVM1IiUXPR6m',
            ],
            [
                'nombre_colegio' => 'Colegio Numero 2',
                'nit_colegio' => '456',
                'direccion' => 'Dirección de Prueba 2',
                'nombre_usuario' => '2',
                'clave' => '$2y$10$WGrQrtF9gVGu9hQFoYQ47eLz/YWABLJ3FO5YF36c9tVM1IiUXPR6m',
            ],
        ]);
    }
}
