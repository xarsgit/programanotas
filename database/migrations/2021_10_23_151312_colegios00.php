<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Colegios00 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('colegios', function (Blueprint $table) {
            $table->id();
            $table->string('nombre_colegio', 70);
            $table->string('nit_colegio', 45);
            $table->string('direccion', 45);
            $table->string('nombre_usuario', 32);
            $table->string('clave', 256);
            $table->timestamps(); // created_at and updated_at
            $table->softDeletes(); // deleted_at
            $table->rememberToken();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('colegios');
    }
}
