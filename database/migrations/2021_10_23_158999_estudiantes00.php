<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Estudiantes00 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estudiantes', function (Blueprint $table) {
            $table->id();
            $table->string('identificacion_estudiante', 20);
            $table->string('nombre_estudiante', 80);
            $table->string('apellido_estudiante', 80);
            $table->enum('sexo_estudiante', ['MASCULINO', 'FEMENINO']);
            $table->date('fecha_nacimiento_estudiante');

            $table->unsignedBigInteger('colegio_id');
            $table->foreign('colegio_id')->references('id')->on('colegios');

            $table->unsignedBigInteger('acudiente_id')->nullable();
            $table->foreign('acudiente_id')->references('id')->on('acudientes');

            $table->unsignedBigInteger('grado_id')->nullable();
            $table->foreign('grado_id')->references('id')->on('grados');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estudiantes');
    }
}
