<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class Docente extends Model
{
    use SoftDeletes;

    protected $table = 'docentes';
    public $timestamps = true;

    protected $fillable = [
        "cedula_docente",
        "nombre_docente",
        "apellido_docente",
        "colegio_id",
    ];

	public function getNombreCompletoDocenteAttribute() {
        return $this->attributes['nombre_docente'] . ' ' . $this->attributes['apellido_docente'];
	}
}
