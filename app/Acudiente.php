<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Acudiente extends Model
{
    use SoftDeletes;

    protected $table = 'acudientes';
    public $timestamps = true;

    protected $fillable = [
        "cedula_acudiente",
        "nombre_acudiente",
        "apellido_acudiente",
        "direccion_residencia",
        "colegio_id",
    ];
    protected $with = ['estudiante', 'telefonos'];

    public function estudiante() {
        return $this->hasMany('App\Estudiante', 'acudiente_id', 'id');
    }
    public function telefonos() {
        return $this->hasMany('App\Telefono', 'acudiente_id', 'id');
    }

	public function getNombreCompletoAcudienteAttribute() {
        return $this->attributes['nombre_acudiente'] . ' ' . $this->attributes['apellido_acudiente'];
	}

}
