<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Telefono extends Model
{
    protected $table = 'telefonos';
    public $timestamps = true;

    protected $fillable = [
        "telefono",
        "acudiente_id",
    ];
}