<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Hash;

class Colegio extends Model implements AuthenticatableContract, CanResetPasswordContract
{
	use Authenticatable, CanResetPassword;
    use SoftDeletes;

    protected $table = 'colegios';
    public $timestamps = true;

    protected $fillable = [
			"nombre_colegio",
			"nit_colegio",
			"direccion",
			'nombre_usuario',
			'clave',
    ];

	protected $hidden = [
		'clave',
		'remember_token'
	];

	public function getAuthPassword() {
		return $this->clave;
	}

	public function setClaveAttribute($value) {
		if(!empty($value)) {
			$this->attributes['clave'] = Hash::make($value);
		}
	}
}
