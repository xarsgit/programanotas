<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Session;
use Redirect;
use Hash;

class ColegioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $colegios = \App\Colegio::all();
        return view('colegios.list', compact('colegios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('colegios.create', compact('colegios'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        \App\Colegio::create(array(
            "nombre_colegio" => $request['nombre_colegio'],
            "nit_colegio" => $request['nit_colegio'],
            "direccion" => $request['direccion'],
        ));
        return Redirect::route('colegio.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Colegio  $colegio
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Colegio  $colegio
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $colegio = \App\Colegio::findOrFail($id);
        return view('colegios.edit', compact('colegio'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Colegio  $colegio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $colegio = \App\Colegio::findOrFail($id);
        $colegio->identificacion_colegio = $request['identificacion_colegio'];
        $colegio->nombre_colegio = $request['nombre_colegio'];
        $colegio->apellido_colegio = $request['apellido_colegio'];
        $colegio->sexo_colegio = $request['sexo_colegio'];
        $colegio->fecha_nacimiento_colegio = $request['fecha_nacimiento_colegio'];
        $colegio->colegio_id = $request['colegio_id'];
        $colegio->save();
        return Redirect::route('colegios.list');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Colegio  $colegio
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        //
    }
}
