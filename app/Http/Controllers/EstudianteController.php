<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;
use Auth;

class EstudianteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $estudiantes = \App\Estudiante::where('colegio_id', Auth::user()->id)->orderBy('nombre_estudiante')->get();
        $search = "";
        return view('estudiantes.list', compact('estudiantes', 'search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $acudientes = \App\Acudiente::where('colegio_id', Auth::user()->id)->orderBy('nombre_acudiente')->get();
        $grados = \App\Grado::where('colegio_id', Auth::user()->id)->orderBy('nombre_grado')->get();
        return view('estudiantes.create', compact('acudientes', 'grados'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        \App\Estudiante::create(array(
            "identificacion_estudiante" => $request['identificacion_estudiante'],
            "nombre_estudiante" => $request['nombre_estudiante'],
            "apellido_estudiante" => $request['apellido_estudiante'],
            "sexo_estudiante" => $request['sexo_estudiante'],
            "fecha_nacimiento_estudiante" => $request['fecha_nacimiento_estudiante'],
            "colegio_id" => Auth::user()->id,
            "acudiente_id" => ($request['acudiente_id'] != -1) ? $request['acudiente_id'] : null,
            "grado_id" => ($request['grado_id'] != -1) ? $request['grado_id'] : null,
        ));
        return Redirect::route('estudiante.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Colegio  $colegio
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Colegio  $colegio
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $estudiante = \App\Estudiante::findOrFail($id);
        $acudientes = \App\Acudiente::where('colegio_id', Auth::user()->id)->orderBy('nombre_acudiente')->get();
        $grados = \App\Grado::where('colegio_id', Auth::user()->id)->orderBy('nombre_grado')->get();
        return view('estudiantes.edit', compact('estudiante', 'acudientes', 'grados'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Colegio  $colegio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $estudiante = \App\Estudiante::findOrFail($id);
        $estudiante->identificacion_estudiante = $request['identificacion_estudiante'];
        $estudiante->nombre_estudiante = $request['nombre_estudiante'];
        $estudiante->apellido_estudiante = $request['apellido_estudiante'];
        $estudiante->sexo_estudiante = $request['sexo_estudiante'];
        $estudiante->fecha_nacimiento_estudiante = $request['fecha_nacimiento_estudiante'];
        $estudiante->acudiente_id = ($request['acudiente_id'] != -1) ? $request['acudiente_id'] : null;
        $estudiante->grado_id = ($request['grado_id'] != -1) ? $request['grado_id'] : null;
        $estudiante->save();
        return Redirect::route('estudiante.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Colegio  $colegio
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $estudiante = \App\Estudiante::findOrFail($id);
        return view('estudiantes.delete', compact('estudiante'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Colegio  $colegio
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $estudiante = \App\Estudiante::findOrFail($id);
        $estudiante->delete();
        return Redirect::route('estudiante.list');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $search = $request['search'];
        $val = '%' . $search . '%';
        $estudiantes = \App\Estudiante::where('colegio_id', Auth::user()->id)->where('identificacion_estudiante', 'like', $val)->orderBy('nombre_estudiante')->get();
        return view('estudiantes.list', compact('estudiantes', 'search'));
    }
}
