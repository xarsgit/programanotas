<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;
use Auth;

class AcudienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $acudientes = \App\Acudiente::where('colegio_id', Auth::user()->id)->orderBy('nombre_acudiente')->get();
        $search = '';
        return view('acudientes.list', compact('acudientes', 'search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $estudiantes = \App\Estudiante::where('colegio_id', Auth::user()->id)->orderBy('nombre_estudiante')->get();
        return view('acudientes.create', compact('estudiantes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $acudiente = \App\Acudiente::create(array(
            "cedula_acudiente" => $request['cedula_acudiente'],
            "nombre_acudiente" => $request['nombre_acudiente'],
            "apellido_acudiente" => $request['apellido_acudiente'],
            "direccion_residencia" => $request['direccion_residencia'],
            "estudiante_id" => $request['estudiante_id'],
            "colegio_id" => Auth::user()->id,
        ));
        \App\Telefono::create(array(
            "telefono" => $request['telefono_1'],
            "acudiente_id" => $acudiente->id,
        ));
        if($request->has('telefono_2') && $request['telefono_2'] != null)
            \App\Telefono::create(array(
                "telefono" => $request['telefono_2'],
                "acudiente_id" => $acudiente->id,
            ));
        return Redirect::route('acudiente.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Colegio  $colegio
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Colegio  $colegio
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $acudiente = \App\Acudiente::findOrFail($id);
        return view('acudientes.edit', compact('acudiente'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Colegio  $colegio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $acudiente = \App\Acudiente::findOrFail($id);
        $acudiente->cedula_acudiente = $request['cedula_acudiente'];
        $acudiente->nombre_acudiente = $request['nombre_acudiente'];
        $acudiente->apellido_acudiente = $request['apellido_acudiente'];
        $acudiente->direccion_residencia = $request['direccion_residencia'];
        $acudiente->save();
        $telefonos = \App\Telefono::where('acudiente_id', $acudiente->id)->get();
        foreach ($telefonos as $t) {
            $t->delete();
        }
        \App\Telefono::create(array(
            "telefono" => $request['telefono_1'],
            "acudiente_id" => $acudiente->id,
        ));
        if($request->has('telefono_2') && $request['telefono_2'] != null)
            \App\Telefono::create(array(
                "telefono" => $request['telefono_2'],
                "acudiente_id" => $acudiente->id,
            ));
        return Redirect::route('acudiente.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Colegio  $colegio
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $acudiente = \App\Acudiente::findOrFail($id);
        return view('acudientes.delete', compact('acudiente'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Colegio  $colegio
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $estudiantes = \App\Estudiante::where('acudiente_id', $id)->get();
        if(count($estudiantes) > 0)
            return "No se puede eliminar un acudiente que tiene de responsable a uno o más estudiantes";
        $acudiente = \App\Acudiente::findOrFail($id);
        $acudiente->delete();
        return Redirect::route('acudiente.index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $search = $request['search'];
        $val = '%' . $search . '%';
        $acudientes = \App\Acudiente::where('cedula_acudiente', 'like', $val)->where('colegio_id', Auth::user()->id)->orderBy('nombre_acudiente')->get();
        return view('acudientes.list', compact('acudientes', 'search'));
    }
}
