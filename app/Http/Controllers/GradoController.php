<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;
use Auth;

class GradoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $grados = \App\Grado::where('colegio_id', Auth::user()->id)->orderBy('nombre_grado')->get();
        return view('grados.list', compact('grados'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $estudiantes = \App\Estudiante::where('colegio_id', Auth::user()->id)->orderBy('nombre_estudiante')->get();
        $docentes = \App\Docente::where('colegio_id', Auth::user()->id)->orderBy('nombre_docente')->get();
        return view('grados.create', compact('estudiantes', 'docentes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request['docente_id'] == -1)
            return "No se puede crear un grado sin docente asignado";
        \App\Grado::create(array(
            "nombre_grado" => $request['nombre_grado'],
            "jornada" => $request['jornada'],
            "docente_id" => $request['docente_id'],
            "colegio_id" => Auth::user()->id,
        ));
        return Redirect::route('grado.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Colegio  $colegio
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $grado = \App\Grado::findOrFail($id);
        $docente = $grado->docente;
        $estudiantes = $grado->estudiantes;
        return view('grados.show', compact('grado', 'docente', 'estudiantes'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Colegio  $colegio
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $grado = \App\Grado::findOrFail($id);
        return view('grados.edit', compact('grado'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Colegio  $colegio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request['docente_id'] == -1)
            return "No se puede actualizar un grado sin docente asignado";
        $grado = \App\Grado::findOrFail($id);
        $grado->nombre_grado = $request['nombre_grado'];
        $grado->jornada = $request['jornada'];
        $grado->docente_id = $request['docente_id'];
        $grado->save();
        return Redirect::route('grado.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Colegio  $colegio
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $grado = \App\Grado::findOrFail($id);
        return view('grados.delete', compact('grado'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Colegio  $colegio
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $estudiantes = \App\Estudiante::where('grado_id', $id)->get()->count();
        if($estudiantes > 0)
            return "No se puede eliminar un grado si contiene estudiantes asignado";
        $grado = \App\Grado::findOrFail($id);
        $grado->delete();
        return Redirect::route('grado.index');
    }
}
