<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;
use Auth;

class DocenteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $docentes = \App\Docente::where('colegio_id', Auth::user()->id)->orderBy('nombre_docente')->get();
        $search = "";
        return view('docentes.list', compact('docentes', 'search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('docentes.create', compact('docentes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $docentes = \App\Docente::where('cedula_docente', $request['cedula_docente'])->get()->count();
        if($docentes > 0)
            return "Ya existe un docente con ese número de cedula";
        \App\Docente::create(array(
            "cedula_docente" => $request['cedula_docente'],
            "nombre_docente" => $request['nombre_docente'],
            "apellido_docente" => $request['apellido_docente'],
            "colegio_id" => Auth::user()->id,
        ));
        return Redirect::route('docente.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Docente  $colegio
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Docente  $colegio
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $docente = \App\Docente::findOrFail($id);
        return view('docentes.edit', compact('docente'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Docente  $colegio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $docente = \App\Docente::findOrFail($id);
        $docente->cedula_docente = $request['cedula_docente'];
        $docente->nombre_docente = $request['nombre_docente'];
        $docente->apellido_docente = $request['apellido_docente'];
        $docente->save();
        return Redirect::route('docente.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Colegio  $colegio
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $docente = \App\Docente::findOrFail($id);
        return view('docentes.delete', compact('docente'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Docente  $colegio
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $grados = \App\Grado::where('docente_id', $id)->get()->count();
        if($grados > 0)
            return "No se puede eliminar un docente que tenga un grado asignado";
        $docente = \App\Docente::findOrFail($id);
        $docente->delete();
        return Redirect::route('docente.index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $search = $request['search'];
        $val = '%' . $search . '%';
        $docentes = \App\Docente::where('cedula_docente', 'like', $val)->where('colegio_id', Auth::user()->id)->orderBy('nombre_docente')->get();
        return view('docentes.list', compact('docentes', 'search'));
    }
}
