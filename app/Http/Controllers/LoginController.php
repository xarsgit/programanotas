<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;
use Redirect;
use Hash;

class LoginController extends Controller
{
    public function index()
    {
        $colegios = \App\Colegio::all();
        if(Auth::check())
            return Redirect::to('/');
        return view('login', compact('colegios'));
    }

    public function check($password) {
        if(Auth::check()) {
            if(Auth::attempt(['nombre_usuario'=>Auth::user()->nombre_usuario, 'password'=>$password])) {
                return Redirect::to('/');
            }
        }
        return Redirect::to('login');
    }

    public function store(Request $request)
    {
        if(Auth::attempt(['nombre_usuario'=>$request['nombre_usuario'], 'password'=>$request['clave']])) {
            return Redirect::to('/');
        }
        return "Contraseña incorrecta (LA CONTRASEÑA ES EL NÚMERO 1)";
        return Redirect::to('login');
    }

    public function logout()
    {
        Auth::logout();
        return Redirect::to('login');
    }
}
