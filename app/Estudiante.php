<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DateTime;

class Estudiante extends Model
{
    use SoftDeletes;

    protected $table = 'estudiantes';
    public $timestamps = true;

    protected $fillable = [
        "identificacion_estudiante",
        "nombre_estudiante",
        "apellido_estudiante",
        "sexo_estudiante",
        "fecha_nacimiento_estudiante",
        "colegio_id",
        "acudiente_id",
        "grado_id",
    ];
    protected $with = ['colegio'];

    public function colegio() {
        return $this->hasOne('App\Colegio', 'id', 'colegio_id');
    }

	public function getEdadAttribute() {
        $date = new DateTime($this->attributes['fecha_nacimiento_estudiante']);
        $now = new DateTime();
        $interval = $now->diff($date);
        return $interval->y;
	}

	public function getNombreCompletoEstudianteAttribute() {
        return $this->attributes['nombre_estudiante'] . ' ' . $this->attributes['apellido_estudiante'];
	}
}
