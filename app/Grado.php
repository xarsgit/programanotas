<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grado extends Model
{

    protected $table = 'grados';
    public $timestamps = true;

    protected $fillable = [
        "nombre_grado",
        "jornada",
        "docente_id",
        "colegio_id",
    ];
    protected $with = ['docente', 'estudiantes'];

    public function docente() {
        return $this->hasOne('App\Docente', 'id', 'docente_id');
    }

    public function estudiantes() {
        return $this->hasMany('App\Estudiante', 'grado_id', 'id');
    }
}

