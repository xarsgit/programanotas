<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	if(Auth::check())
		return Redirect::route('estudiante.index');
	return Redirect::to('login');
});

Route::get('/login', 'LoginController@index')->name('login.index');
Route::post('/login', 'LoginController@store')->name('login.store');
Route::get('/logout', 'LoginController@logout')->name('logout');

// Route::get('/principal', function() {
//     return view('principal');
// })->name('principal');

Route::get('/estudiante', 'EstudianteController@index')->name('estudiante.index');
Route::get('/estudiante/create', 'EstudianteController@create')->name('estudiante.create');
Route::post('/estudiante/store', 'EstudianteController@store')->name('estudiante.store');
Route::get('/estudiante/{id}/edit', 'EstudianteController@edit')->name('estudiante.edit');
Route::post('/estudiante/{id}/update', 'EstudianteController@update')->name('estudiante.update');
Route::get('/estudiante/{id}/delete', 'EstudianteController@delete')->name('estudiante.delete');
Route::post('/estudiante/{id}/destroy', 'EstudianteController@destroy')->name('estudiante.destroy');
Route::post('/estudiante/search', 'EstudianteController@search')->name('estudiante.search');

Route::get('/acudiente', 'AcudienteController@index')->name('acudiente.index');
Route::get('/acudiente/create', 'AcudienteController@create')->name('acudiente.create');
Route::get('/acudiente/{id}/edit', 'AcudienteController@edit')->name('acudiente.edit');
Route::post('/acudiente/store', 'AcudienteController@store')->name('acudiente.store');
Route::post('/acudiente/{id}/update', 'AcudienteController@update')->name('acudiente.update');
Route::get('/acudiente/{id}/delete', 'AcudienteController@delete')->name('acudiente.delete');
Route::post('/acudiente/{id}/destroy', 'AcudienteController@destroy')->name('acudiente.destroy');
Route::post('/acudiente/search', 'AcudienteController@search')->name('acudiente.search');

Route::get('/colegio', 'ColegioController@index')->name('colegio.index');
Route::get('/colegio/create', 'ColegioController@create')->name('colegio.create');
Route::get('/colegio/{id}/edit', 'ColegioController@edit')->name('colegio.edit');
Route::post('/colegio/store', 'ColegioController@store')->name('colegio.store');
Route::post('/colegio/update', 'ColegioController@update')->name('colegio.update');

Route::get('/docente', 'DocenteController@index')->name('docente.index');
Route::get('/docente/create', 'DocenteController@create')->name('docente.create');
Route::get('/docente/{id}/edit', 'DocenteController@edit')->name('docente.edit');
Route::post('/docente/store', 'DocenteController@store')->name('docente.store');
Route::post('/docente/{id}/update', 'DocenteController@update')->name('docente.update');
Route::get('/docente/{id}/delete', 'DocenteController@delete')->name('docente.delete');
Route::post('/docente/{id}/destroy', 'DocenteController@destroy')->name('docente.destroy');
Route::post('/docente/search', 'DocenteController@search')->name('docente.search');

Route::get('/grado', 'GradoController@index')->name('grado.index');
Route::get('/grado/create', 'GradoController@create')->name('grado.create');
Route::get('/grado/{id}/show', 'GradoController@show')->name('grado.show');
Route::get('/grado/{id}/edit', 'GradoController@edit')->name('grado.edit');
Route::post('/grado/store', 'GradoController@store')->name('grado.store');
Route::post('/grado/{id}/update', 'GradoController@update')->name('grado.update');
Route::get('/grado/{id}/delete', 'GradoController@delete')->name('grado.delete');
Route::post('/grado/{id}/destroy', 'GradoController@destroy')->name('grado.destroy');